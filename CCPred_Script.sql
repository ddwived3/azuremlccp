
CREATE TABLE [dbo].[Customers] (
    [CustomerId] INT           NOT NULL,
    [FName]      NVARCHAR (30) NULL,
    [LName]      NVARCHAR (30) NULL,
    [Location]   NVARCHAR (25) NULL,
    [ZipCode]    NVARCHAR (15) NULL,
    [Gender]     CHAR (1)      NULL,
	[Age]     	 INT      	   NULL,
    [IncomeBand] CHAR (1)      NULL,
	/*
		1- Less than 10,000
		2- 10,001 to 50,000
		3- 50,001 to 100,000
		4- 100,001 to 200,000
		5 - Above 200,001
	*/
    PRIMARY KEY CLUSTERED ([CustomerId] ASC)
);

CREATE TABLE [dbo].[ComplaintsRecorded] (
    [CustomerId]           INT            NOT NULL,
    [ChannelCode]          INT            NOT NULL,
    [ComplaintId]          INT            NOT NULL,
    [ComplaintDescription] NVARCHAR (250) NULL,
    [SatisfactionalLevel]  NVARCHAR (15)  NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [ChannelCode] ASC, [ComplaintId] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[CustomerExitStatus] (
    [CustomerId] INT  NOT NULL,
    [AsOfDate]   DATE NOT NULL,
    [ExitFlag]   BIT  NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [AsOfDate] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[OutstandingBalanceInDepositProducts] (
    [CustomerId]       INT          NOT NULL,
    [DepositProdCode]  VARCHAR (3)  NOT NULL,
    [AvailableBalance] DECIMAL (18) NULL,
    [AsOfDate]         DATE         NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [DepositProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[OutstandingBalanceInLoanProducts] (
    [CustomerId]         INT          NOT NULL,
    [LoanProdCode]       VARCHAR (3)  NOT NULL,
    [OutstandingBalance] DECIMAL (18) NULL,
    [AsOfDate]           DATE         NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [LoanProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[PaymentsInLoanProducts] (
    [CustomerId]      INT          NOT NULL,
    [ProdCode]        VARCHAR (3)  NOT NULL,
    [TransactionId]   INT          NOT NULL,
    [PaymentDate]     DATE         NULL,
    [PaymentAmount]   DECIMAL (18) NULL,
    [DueDateSliped]   BIT          NULL,
    [InterestApplied] BIT          NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [TransactionId] ASC, [ProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[Products] (
    [CustomerId] INT         NOT NULL,
    [ProdCode]   VARCHAR (3) NOT NULL,
    [IsActive]   BIT         NULL,
    [AsOfDate]   DATE        NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [ProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[ProductsApplicationStatus] (
    [CustomerId]        INT         NOT NULL,
    [ProdCode]          VARCHAR (3) NOT NULL,
    [ApplicationStatus] CHAR (1)    DEFAULT ('P') NULL,
    [AppliedDate]       DATE        NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [ProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[SentimentAnalysisScores] (
    [CustomerId]  INT        NOT NULL,
    [ChannelCode] INT        NOT NULL,
    [AsOfDate]    DATE       NOT NULL,
    [Score]       FLOAT (53) NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [ChannelCode] ASC, [AsOfDate] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);

CREATE TABLE [dbo].[TransactionHistoryInDepositProducts] (
    [CustomerId]          INT          NOT NULL,
    [TransactionId]       INT          NOT NULL,
    [DepositProdCode]     VARCHAR (3)  NOT NULL,
    [TransactionalAmount] DECIMAL (18) NULL,
    [TransactionDate]     DATE         NULL,
    PRIMARY KEY CLUSTERED ([CustomerId] ASC, [TransactionId] ASC, [DepositProdCode] ASC),
    FOREIGN KEY ([CustomerId]) REFERENCES [dbo].[Customers] ([CustomerId])
);